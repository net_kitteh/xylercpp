#include <iostream>
#include <string.h>

//char  memory
char* plaintext;
char* key;
char* middleman;

//simple structure for storing points
struct point{
  int x,y;
};

//point memory
point* l1ciphertext,* l1key,* l2ciphertext;

//INIT_TABLE = convert each letter in the plaintext into points (x,y coords) with this table
char INIT_TABLE[6][6] = {
		     {'a','b','c','d','e','f'},
		     {'g','h','i','j','k','l'},
		     {'m','n','o','p','q','r'},
		     {'s','t','u','v','w','x'},
		     {'y','z','0','1','2','3'},
		     {'4','5','6','7','8','9'}
		    };

//TABULA RECTA = subsitute the indexes in point objects with this table
int TABULA_RECTA[6][6] = {
		     {1,2,3,4,5,0},
		     {2,3,4,5,0,1},
		     {3,4,5,0,1,2},
		     {4,5,0,1,2,3},
		     {5,0,1,2,3,4},
		     {0,1,2,3,4,5}
		    };

//find a char in the INIT_TABLE
point findChar(char ARG){
  for(int row = 0; row < 6; row++){
   for(int col = 0; col < 6; col++){
     
     if(INIT_TABLE[row][col] == ARG){
       
	point p;
	p.x = col;
	p.y = row;
	
	return p;
      }
      
    } 
  } 
}

//find an int in the TABULA_RECTA
int findPoint(int x, int y){
  return TABULA_RECTA[y][x];
}

//large functions, implemented under main
void encode();
void decode();

int main(int argc, char **argv) {
  
  while(true){
    //pull user input
    std::cout << "enter 1 to encode text, enter 2 to decode text, enter 3 to exit\n >";
    int choice;
    std::cin >> choice;
    std::cin.ignore();
    
    
    //switchboard
    switch(choice){
      case 1:
	encode();
	break;
      case 2:
	decode();
	break;
      case 3:
	return 0;
      default:
	std::cout << "invalid option." << std::endl;
    }
  }
  
  return 0;
}

void decode(){
  //get ciphertext via middleman
  middleman = new char[65335];
  std::cout << "enter ciphertext\n >";
  std::cin.getline(middleman, 65335);
  
  //initialize memory
  int init_val = strlen(middleman);
  l1ciphertext = new point[init_val];
  l2ciphertext = new point[init_val];
  key = new char[init_val];
  
  //copy memory from middleman to l2cyphertext
  for(int i = 0; i < init_val; i++){
    l2ciphertext[i] = findChar(middleman[i]);
        
  }
  
  //delete middleman, accept key
  delete[] middleman;
  middleman = new char[65335];
  std::cout << "input key\n >";
  std::cin.getline(middleman, 65335);
  
  //calculate length of key
  int keylen = strlen(middleman);
  int keyiter = 0;
  
  //middleman -> key
  for (int i = 0; i < init_val; ++i){
    
    //catch out of bounds and loop back to beginning of key, stored in middleman
    if(keyiter == keylen){
      keyiter = keyiter - keylen;
    } 
    
    //transfer memory
    
    //uppercase to lowercase
      if(isalpha(middleman[keyiter])){
	
	if(isupper(middleman[keyiter])){
	  
	  middleman[keyiter] = tolower(middleman[keyiter]);
	}
      }
      
    key[i] = middleman[keyiter];
    ++keyiter;
  }
  delete[] middleman;
  
  //================================encode key + decode ciphertext========================
  
  for (int i = 0; i < init_val; ++i){
      
    //encode point in key
    point keypoint = findChar(key[i]);
    
    //find point from l2ciphertext and keyiter
    int x, y;
    for (int index = 0; index < 6; ++index){
      if (TABULA_RECTA[keypoint.x][index] == l2ciphertext[i].x){
	x = index;
      }
    }
    
    for (int index = 0; index < 6; ++index){
      if (TABULA_RECTA[keypoint.y][index] == l2ciphertext[i].y){
	y = index;
      }
    }
    
    //gen point
    point l1point;
    l1point.x = x;
    l1point.y = y;
    
    l1ciphertext[i] = l1point;
  }
  
  //=====================================decode plaintxt==================================
  
  for(int i = 0; i < init_val; ++i){
    std::cout << INIT_TABLE [ l1ciphertext[i].y ] [ l1ciphertext[i].x ];
  }
  std::cout << std::endl;
  
  return;
}

void encode(){
  
  //get plaintext via middleman
  middleman = new char[65335];
  std::cout << "enter plaintext\n >";
  std::cin.getline(middleman, 65335);
  
  int init_val = 0; //modified strlen() of middleman
  //count only letters/number
  for (int i = 0; i < strlen(middleman); ++i){
    
    if(isalnum(middleman[i])){
      init_val++;
    }
  }
  
  //initialize plaintext container
  plaintext = new char[init_val];  
  int plaintext_index = 0;
  
  //copy mem to plaintext container
  for (int i = 0; i < strlen(middleman); ++i){
    
    if(isalnum(middleman[i])){
      
      //uppercase to lowercase
      if(isalpha(middleman[i])){
	
	if(isupper(middleman[i])){
	  
	  middleman[i] = tolower(middleman[i]);
	}
      }
      
      plaintext[plaintext_index] = middleman[i];
      ++plaintext_index;
    }
  } 
  
  //destroy middleman
  delete[] middleman;
  
  //initialize memory
  l1ciphertext = new point[init_val];
  l1key = new point[init_val];
  l2ciphertext = new point[init_val];
  key = new char[init_val];
  
  //reinstate middleman for the key
  middleman = new char[65335];
  
  //get key
  std::cout << "input key\n >";
  std::cin.getline(middleman, 65335);
  
  //calculate length of key
  int keylen = strlen(middleman);
  int keyiter = 0;
  
  for (int i = 0; i < init_val; ++i){
    
    //catch out of bounds and loop back to beginning of key, stored in middleman
    if(keyiter == keylen){
      keyiter = keyiter - keylen;
    } 
    
    //transfer memory
    
    //uppercase to lowercase
      if(isalpha(middleman[keyiter])){
	
	if(isupper(middleman[keyiter])){
	  
	  middleman[keyiter] = tolower(middleman[keyiter]);
	}
      }
      
    key[i] = middleman[keyiter];
    ++keyiter;
  }
  
  //delete middleman
  delete[] middleman;
  
  //==================================encode l1======================================
  
  for (int i = 0; i < init_val; ++i){
    
    l1ciphertext[i] = findChar(plaintext[i]);
    
    l1key[i] = findChar(key[i]);
    
  }
  
  //==================================encode l2======================================
  
  for (int i = 0; i < init_val; ++i){
    
    point* x_point,* y_point;
    x_point = &l1ciphertext[i];
    y_point = &l1key[i];
    
    point l2;
    l2.x = findPoint(x_point->x, y_point->x);
    l2.y = findPoint(x_point->y, y_point->y);
    
    l2ciphertext[i] = l2;
  }
  
  //==================================output ct======================================
  
  for (int i = 0; i < init_val; ++i){
    std::cout << INIT_TABLE [ l2ciphertext[i].y ] [ l2ciphertext[i].x ];
  }
  std::cout << std::endl;
  
  return;
}
